package org.jbalance.directory.address.core;

import java.util.List;
import java.util.UUID;


/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface IAddressFinder
{
    int CITY_SELECT_LIMIT = 20;
    int HOUSE_SELECT_LIMIT = 20;
    int STREET_SELECT_LIMIT = 20;

    List<ICity> findCity(String term);

    ICity findCity(UUID uuid);

    List<IHouse> findHouse(String term, UUID parent);

    List<IHouseInterval> findHouseInterval(UUID parent);

    List<IStreet> findStreet(String term, UUID parent);

    IStreet findStreet(UUID uuid);

    String getFullAddressForAddressObject(UUID uuid);

    String getFullAddressForHouse(UUID uuid, int regionCode);

    String getFullAddressForHouseInterval(UUID uuid, String houseNum);

}
