package org.jbalance.directory.address.objects;

import java.io.Serializable;
import org.jbalance.directory.address.core.ICity;
import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class City extends AddressObject implements ICity, Serializable
{

    public City()
    {      
    }

    public City(UUID uuid)
    {
        super(uuid);
    }
}
