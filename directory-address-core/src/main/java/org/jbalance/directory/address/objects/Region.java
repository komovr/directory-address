package org.jbalance.directory.address.objects;

import org.jbalance.directory.address.core.IRegion;
import java.util.UUID;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class Region extends AddressObject implements IRegion
{

    public Region()
    {
    }

    public Region(UUID uuid)
    {
        super(uuid);
    }

    @Override
    public String toString()
    {
        return this.formalName;
    }
}
