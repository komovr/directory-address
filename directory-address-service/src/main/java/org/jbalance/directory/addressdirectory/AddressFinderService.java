package org.jbalance.directory.addressdirectory;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.naming.NamingException;
import javax.xml.ws.WebServiceContext;
import org.jbalance.directory.address.client.AddressFinderClient;
import org.jbalance.directory.address.core.IAddressFinder;
import org.jbalance.directory.address.core.ICity;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@WebService(serviceName = "AddressFinderService")
public class AddressFinderService
{

    @Resource
    private WebServiceContext context;

    @WebMethod(operationName = "findCityByTerm")
    public String findCity(@WebParam(name = "term") String term)
    {
        List<ICity> l = getAddressFinder().findCity(term);

        return l.get(0).toString();
    }
    @WebMethod(operationName = "getFullAddressForAddressObject")
    public String getFullAddressForAddressObject(@WebParam(name = "term") UUID uuid)
    {
        return getAddressFinder().getFullAddressForAddressObject(uuid);
    }

    private IAddressFinder getAddressFinder()
    {
        AddressFinderClient afc = new AddressFinderClient();

        IAddressFinder i = null;
        try
        {
            i = afc.lookupAddressFinder();
        } catch (NamingException ex)
        {
            Logger.getLogger(AddressFinderService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return i;
    }
}
