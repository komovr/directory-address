package org.jbalance.directory.fias;

import org.jbalance.directory.address.core.IAddressFinder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class AddressFinderManager
{

    String driverName;

    String connString;

    Connection connection;

    public AddressFinderManager(Properties properties)
    {
        // get the property value and print it out
        driverName = properties.getProperty("driverName");
        connString = properties.getProperty("connString");
    }

    public IAddressFinder getFinder()
    {
        try
        {
            Class.forName(driverName);
            connection = DriverManager.getConnection(connString);
            return new AddressFinder(connection);

        } catch (ClassNotFoundException ex)
        {
            Logger.getLogger(AddressFinderManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SQLException ex)
        {
            Logger.getLogger(AddressFinderManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Connection getConnection()
    {
        return connection;
    }
}
