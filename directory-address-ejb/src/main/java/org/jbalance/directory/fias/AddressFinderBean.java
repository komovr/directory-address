package org.jbalance.directory.fias;

import java.util.ArrayList;
import org.jbalance.directory.address.core.IAddressFinder;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import org.jbalance.directory.address.core.ICity;
import org.jbalance.directory.address.core.IHouse;
import org.jbalance.directory.address.core.IHouseInterval;
import org.jbalance.directory.address.core.IStreet;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import org.jbalance.directory.address.objects.City;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@Stateless
@Remote(IAddressFinder.class)
public class AddressFinderBean implements IAddressFinder
{

    public List<ICity> findCity(String term)
    {
        return getAddressFinder().findCity(term);
    }

    public ICity findCity(UUID uuid)
    {
        return getAddressFinder().findCity(uuid);
    }

    public List<IStreet> findStreet(String term, UUID cityUUID)
    {
        return getAddressFinder().findStreet(term, cityUUID);
    }

    public IStreet findStreet(UUID uuid)
    {
        return getAddressFinder().findStreet(uuid);
    }

    public List<IHouse> findHouse(String term,
            UUID streetUUID)
    {
        IAddressFinder af = getAddressFinder();

        return af.findHouse(term, streetUUID);
    }

    @Override
    public List<IHouseInterval> findHouseInterval(UUID parentUUID)
    {
        IAddressFinder af = getAddressFinder();

        return af.findHouseInterval(parentUUID);
    }

    @Override
    public String getFullAddressForAddressObject(UUID uuid)
    {
        IAddressFinder af = getAddressFinder();

        return af.getFullAddressForAddressObject(uuid);
    }

    @Override
    public String getFullAddressForHouse(UUID uuid, int regionCode)
    {
        IAddressFinder af = getAddressFinder();

        return af.getFullAddressForHouse(uuid, regionCode);
    }

    @Override
    public String getFullAddressForHouseInterval(UUID uuid, String houseNum)
    {
        IAddressFinder af = getAddressFinder();

        return af.getFullAddressForHouseInterval(uuid, houseNum);
    }

    private IAddressFinder getAddressFinder()
    {

        //TODO: use datasource instead
        Properties props = new Properties();
        props.setProperty("driverName", "org.postgresql.Driver");
        props.setProperty("connString", "jdbc:postgresql://10.87.0.98:5432/fias_db?user=postgres&password=root");

        IAddressFinder af = (new AddressFinderManager(props)).getFinder();

        return af;
    }
}
