package org.jbalance.directory.fias;

import org.jbalance.directory.address.core.IAddressFinder;
import org.jbalance.directory.address.objects.City;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.jbalance.directory.address.core.ICity;
import org.jbalance.directory.address.core.IHouse;
import org.jbalance.directory.address.core.IHouseInterval;
import org.jbalance.directory.address.core.IStreet;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class Main
{

    public static void main(String[] args)
    {

        String city = "Новошахтинск";
        String street = "Х";
        String house = "";

        Properties prop = null;

        prop = getProperties("db.properties");

        AddressFinderManager mgr = new AddressFinderManager(prop);
        IAddressFinder adr = mgr.getFinder();

        List<ICity> c = adr.findCity(city);
        Iterator<ICity> i = c.iterator();
        while (i.hasNext())
        {
            City _c = (City) i.next();
            //System.out.println(_c);
            System.out.println(adr.getFullAddressForAddressObject(_c.getAoGuid()));

            List<IStreet> s = adr.findStreet(street, _c.getAoGuid());
            Iterator<IStreet> is = s.iterator();
            while (is.hasNext())
            {
                IStreet _s = is.next();
                //System.out.println(_s);
                System.out.println(adr.getFullAddressForAddressObject(_s.getAoGuid()));

                List<IHouse> h = adr.findHouse(house, _s.getAoGuid());
                Iterator<IHouse> ih = h.iterator();
                while (ih.hasNext())
                {
                    IHouse __h = ih.next();

                    //System.out.println(__h);
                    System.out.println(adr.getFullAddressForHouse(__h.getHouseGuid(), _s.getRegionCode()));
                }

                List<IHouseInterval> hi = adr.findHouseInterval(_s.getAoGuid());
                Iterator<IHouseInterval> ihi = hi.iterator();
                while (ihi.hasNext())
                {
                    IHouseInterval __hi = ihi.next();
                    //System.out.println(ihi.next());
                    System.out.println(adr.getFullAddressForHouseInterval(__hi.getHouseIntId(), "1"));
                }
            }
        }
    }

    private static Properties getProperties(String propFileName)
    {
        Properties props = new Properties();
        props.setProperty("driverName", "org.postgresql.Driver");
        props.setProperty("connString", "jdbc:postgresql://10.87.0.98:5432/fias_db?user=postgres&password=root");
        return props;
    }

}
