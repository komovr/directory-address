package org.jbalance.directory.fias;

import org.jbalance.directory.address.core.IAddressFinder;
import org.jbalance.directory.address.objects.Street;
import org.jbalance.directory.address.core.ICity;
import org.jbalance.directory.address.objects.City;
import org.jbalance.directory.address.objects.House;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.jbalance.directory.address.core.IAddressObject;
import org.jbalance.directory.address.core.IHouse;
import org.jbalance.directory.address.core.IHouseInterval;
import org.jbalance.directory.address.core.IStreet;
import org.jbalance.directory.address.objects.AddressObject;
import org.jbalance.directory.address.objects.HouseInterval;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class AddressFinder implements IAddressFinder
{

    Connection connection = null;

    public AddressFinder(Connection c)
    {
        connection = c;
    }

    @Override
    public List<ICity> findCity(String term)
    {
        List result = new ArrayList<ICity>();
        PreparedStatement preparedStatement = null;

        String sql = "SELECT * FROM ADDROBJ where FORMALNAME like ? "
                + "AND AOLEVEL IN (?,?) and ACTSTATUS=? ";// usual sql query
        try
        {

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, term + "%");
            preparedStatement.setString(2, "4");
            preparedStatement.setString(3, "6");
            preparedStatement.setString(4, "1");
            //preparedStatement.setInt(5, AddressFinder.CITY_SELECT_LIMIT);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                ICity c = (ICity) mapAddressObject(resultSet, new City());

                result.add(c);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ICity findCity(UUID uuid)
    {
        ICity c = null;
        PreparedStatement preparedStatement = null;

        String sql = "SELECT * FROM ADDROBJ where AOGUID = ?  ";
        try
        {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                c = (ICity) mapAddressObject(resultSet, new City());
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return c;
    }

    @Override
    public List<IStreet> findStreet(String term, UUID parent)
    {
        List result = new ArrayList<Street>();
        PreparedStatement preparedStatement = null;

        String sql = "SELECT * FROM ADDROBJ where FORMALNAME like ? AND PARENTGUID=?  AND  ACTSTATUS=? ";

        try
        {
            preparedStatement = connection.prepareStatement(sql);

            //search term
            preparedStatement.setString(1, term + "%");

            //parent
            preparedStatement.setString(2, parent.toString());

            //actual status
            preparedStatement.setString(3, "1");

            //limit
            //preparedStatement.setInt(3, AddressFinder.STREET_SELECT_LIMIT);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                Street s = (Street) mapAddressObject(resultSet, new Street());

                result.add(s);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public IStreet findStreet(UUID uuid)
    {
        IStreet s = null;
        PreparedStatement preparedStatement = null;

        String sql = "SELECT * FROM ADDROBJ where AOGUID = ?  ";
        try
        {

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                s = (Street) mapAddressObject(resultSet, new Street());
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public List<IHouse> findHouse(String term, UUID parent)
    {
        List result = new ArrayList<House>();
        PreparedStatement preparedStatement = null;
        IStreet street = this.findStreet(parent);
        String tableName = getHouseTableName(street.getRegionCode());

        String sql = "SELECT * FROM " + tableName + " where HOUSENUM like ? AND AOGUID=? and ENDDATE<NOW()";

        try
        {

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, term + "%");
            preparedStatement.setString(2, parent.toString());
            //preparedStatement.setInt(3, Street.ADDRESS_LEVEL_ID);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                House s = new House(UUID.fromString(resultSet.getString("HOUSEGUID")), street.getRegionCode());
                s.setHouseNum(resultSet.getString("HOUSENUM"));
                s.setAoGuid(UUID.fromString(resultSet.getString("AOGUID")));

                result.add(s);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public String getFullAddressForHouse(UUID uuid, int regionCode)
    {
        PreparedStatement preparedStatement = null;
        StringBuilder sb = new StringBuilder();
        String tableName = getHouseTableName(regionCode);

        String sql = "SELECT * FROM " + tableName + " where HOUSEGUID = ?";

        try
        {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
            {
                sb.append(resultSet.getString("HOUSENUM")).append(", ");
                sb.append(getFullAddressForAddressObject(UUID.fromString(resultSet.getString("AOGUID"))));

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public String getFullAddressForHouseInterval(UUID uuid, String houseNum)
    {
        PreparedStatement preparedStatement = null;
        StringBuilder sb = new StringBuilder();

        String sql = "SELECT * FROM HOUSEINT where HOUSEINTID = ?";

        try
        {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
            {
                sb.append(houseNum).append(", ");
                sb.append(getFullAddressForAddressObject(UUID.fromString(resultSet.getString("AOGUID"))));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @Override
    public String getFullAddressForAddressObject(UUID uuid)
    {
        StringBuilder sb = new StringBuilder();
        PreparedStatement preparedStatement = null;

        String sql = "SELECT * FROM ADDROBJ where AOGUID = ?  ";
        try
        {
            while (true)
            {
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, uuid.toString());

                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next())
                {
                    sb
                            .append(resultSet.getString("SHORTNAME"))
                            .append(" ")
                            .append(resultSet.getString("FORMALNAME"))
                            .append(" ");
                    String _parentGuid = resultSet.getString("PARENTGUID");
                    if (!_parentGuid.equals(""))
                    {
                        uuid = UUID.fromString(resultSet.getString("PARENTGUID"));
                        continue;
                    }
                    break;
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return sb.toString();
    }

    @Override
    public List<IHouseInterval> findHouseInterval(UUID parent)
    {
        List<IHouseInterval> result = new ArrayList<IHouseInterval>();
        PreparedStatement preparedStatement = null;

        String sql = "SELECT * FROM HOUSEINT where AOGUID = ? and ENDDATE<NOW()";// usual sql query
        try
        {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, parent.toString());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                HouseInterval s = new HouseInterval(UUID.fromString(resultSet.getString("INTGUID")));
                s.setIntStart(resultSet.getInt("INTSTART"));
                s.setIntEnd(resultSet.getInt("INTEND"));
                s.setIntStatus(resultSet.getInt("INTSTATUS"));
                s.setAoGuid(UUID.fromString(resultSet.getString("AOGUID")));

                result.add(s);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    protected String getHouseTableName(int regionCode)
    {
        return "HOUSE" + String.valueOf(regionCode);
    }

    protected IAddressObject mapAddressObject(ResultSet r, AddressObject o) throws SQLException
    {

        //TODO: use  mapping lib
        o.setAoGuid(UUID.fromString(r.getString("AOGUID")));
        o.setFormalName(r.getString("FORMALNAME"));
        o.setParentGuid(UUID.fromString(r.getString("PARENTGUID")));
        o.setRegionCode(r.getInt("REGIONCODE"));
        o.setActStatus(r.getInt("ACTSTATUS"));
        o.setShortName(r.getString("SHORTNAME"));

        return o;
    }
}
