package org.jbalance.directory.address.client;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import org.jbalance.directory.address.core.IAddressFinder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class AddressFinderClient extends BaseClient
{

    /**
     *
     * @return IAddressFinder
     * @throws NamingException
     */
    public IAddressFinder lookupAddressFinder() throws NamingException
    {
        return (IAddressFinder) lookup("java:global/directory-address-ear-1.0-SNAPSHOT/directory-address-ejb-1.0-SNAPSHOT/AddressFinderBean!org.jbalance.directory.address.core.IAddressFinder");

    }
}
